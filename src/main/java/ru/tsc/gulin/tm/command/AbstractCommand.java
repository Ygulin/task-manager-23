package ru.tsc.gulin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.model.ICommand;
import ru.tsc.gulin.tm.api.service.IAuthService;
import ru.tsc.gulin.tm.api.service.IServiceLocator;
import ru.tsc.gulin.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void execute();

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public String getUserId() {
        return getAuthService().getUserId();
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += " - " + description;
        return result;
    }

}
