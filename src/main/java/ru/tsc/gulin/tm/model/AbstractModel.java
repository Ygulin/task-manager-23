package ru.tsc.gulin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class AbstractModel {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
