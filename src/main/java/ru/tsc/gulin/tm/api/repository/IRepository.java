package ru.tsc.gulin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M add(@NotNull M model);

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M remove(@NotNull M model);

    @NotNull
    M removeByIndex(@NotNull Integer index);

    @Nullable
    M removeById(@NotNull String id);

    void removeAll(@NotNull Collection<M> collection);

    void clear();

    boolean existsById(String id);

    long getSize();

}
